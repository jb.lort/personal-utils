Cross compilation
=================

Toolchains
----------

It's often cumbersome to use cross-compilation toolchains in various project
especially when one needs to access these tools from the CLI easily.

A simple bash function per toolchain fixes this issue:

    export <name>_TOOLCHAIN_PREFIX="<path>/<to>/<toolchain>/<bindir>"
    
    function <name>-toolchain() {
        if [[ -z "$1" ]]; then
            echo "Usage: $0 toolname"
            return 1;
        fi
        tool=$1
        shift 1
        $<name>_TOOLCHAIN_PREFIX$tool $@
    }

This allows easily invoking specific tools from the CLI like so:

    <name>_toolchain nm <args>

Replace `<name>` with a name for the toolchain, and duplicate for each
needed toolchain.

Caveat: if the toolchain isn't somehow in the path, there might be issues with
certain tools that expect it. Seems to work with `nm`, `objdump`, etc. but for 
example `ldd` complains that its root is not set.
