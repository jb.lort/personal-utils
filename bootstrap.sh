#!/bin/sh

# A setup script that installs and configures most of
# the tools and configs contained in this repository
# to minimize the manual inputs when setuping a new
# machine

# 1. Setup zsh

# 2. Setup vim

# 3. Setup git

# 4. Setup CLI tools (fzf, ack, autojump, diff-so-fancy, etc.)
