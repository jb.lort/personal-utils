call plug#begin()

" Libraries / dependencies
Plug 'kyazdani42/nvim-web-devicons'
Plug 'williamboman/mason.nvim'
Plug 'williamboman/mason-lspconfig.nvim'
Plug 'folke/neodev.nvim'
Plug 'nvim-lualine/lualine.nvim'

" Colorscheme
Plug 'sainnhe/sonokai'

" LSP
Plug 'neovim/nvim-lspconfig'
Plug 'folke/lsp-colors.nvim'
Plug 'ray-x/lsp_signature.nvim'
" Plug 'ray-x/go.nvim'
Plug 'ray-x/guihua.lua'

" Treesitter
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}

" Trouble
Plug 'folke/trouble.nvim'

" cmp
Plug 'hrsh7th/cmp-nvim-lsp'
Plug 'hrsh7th/cmp-buffer'
Plug 'hrsh7th/cmp-path'
Plug 'hrsh7th/cmp-cmdline'
Plug 'hrsh7th/nvim-cmp'

" For vsnip users.
Plug 'hrsh7th/cmp-vsnip'
Plug 'hrsh7th/vim-vsnip'

" Comments
Plug 'tpope/vim-commentary'

" Git
Plug 'tpope/vim-fugitive'

" Markdown preview
Plug 'iamcco/markdown-preview.nvim', { 'do': 'cd app && npx --yes yarn install'}

" Telescope
Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-telescope/telescope.nvim'

" File explorer
Plug 'nvim-tree/nvim-tree.lua'

Plug 'rmagatti/goto-preview'

Plug 'numToStr/Comment.nvim'

Plug 'onsails/lspkind-nvim'

Plug 'p00f/clangd_extensions.nvim'

Plug 'L3MON4D3/LuaSnip'

Plug 'ntpeters/vim-better-whitespace'

Plug 'peterhoeg/vim-qml'

call plug#end()

" The configuration options should be placed before `colorscheme sonokai`.
let g:sonokai_style = 'shusia'
let g:sonokai_better_performance = 1
colorscheme sonokai

" Consistency with terminal colors
if has('termguicolors')
  set termguicolors
endif

set shell=/bin/zsh
set shellcmdflag=-c

""""""""""""""""""""""""""
" INDENTATION MANAGEMENT "
""""""""""""""""""""""""""
set autoindent
set smartindent
filetype plugin indent on
" show existing tab with 4 spaces width
set tabstop=4
" when indenting with '>', use whatever tabstop is set to
set shiftwidth=0
set softtabstop=-1
" keep C++ visibility accessor (public/private/protected) on the same level
" as the block braces
set cino+=g0
" On pressing tab, insert spaces by default
set expandtab

" Use the system clipboard
set clipboard=unnamedplus

" Read gltf as Json
autocmd BufRead,BufNewFile *.gltf set filetype=json

" Autoformat Go files
autocmd BufWritePost *.go GoFmt

" Filetypes we don't want to open with nvim
set wildignore=.svn,CVS,*.o,*.a,*.class,*.mo,*.la,*.so,*.obj,*.swp,*.jpg,*.png,*.xpm,*.gif

" Various settings
set autochdir      " CWD is always same as current file
set ai             " Autoident
set nowrap         " Do not wrap lines
set smartcase      " Smart casing when searching
set history=500    " Long undo history
set tw=1000
set scrolloff=3    " Ensure there are 1 line at least above / below the cursor when moving
set smartcase      " Searches are case sensitive only if there are capitals


"Misc editor properties
set showcmd         "Show last command in bottom right corner
set cursorline      "Highlight line on which cursor is standing
let c_space_errors = 1
let c_no_trail_space_error = 1

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" User interface setings
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
set showmatch                        " Show matching braces when over one
set ruler                            " Always show current position
set number                           " Always show line-numbers
set numberwidth=5                    " Line-number margin width
set mousehide                        " Do not show mouse while typing
set linespace=0                      " Don't insert any extra pixel lines
set relativenumber
set colorcolumn=80,120

"Better split opening location defaults
set splitbelow
set splitright


augroup toggle_relative_number
    autocmd InsertEnter * :setlocal norelativenumber
    autocmd InsertLeave * :setlocal relativenumber
augroup END

" ================= Custom bindings =================
let mapleader= "\<space>"
map <space> <Leader>

"Remap leader + space to stop highlighting of the last search
nnoremap <Leader>h :nohlsearch<CR>

"Remap arrows to be inoperant. Get used to hjkl
noremap <Up> <nop>
noremap <Down> <nop>
noremap <Left> <nop>
noremap <Right> <nop>
inoremap <Up> <nop>
inoremap <Down> <nop>
inoremap <Left> <nop>
inoremap <Right> <nop>

"Bind j and k to move up/down one "logical" line.
nnoremap j gj
nnoremap k gk

"Bind r to an actual "replace" that doesn't overwrite the unnamed register
vmap r "_dP

"navigation between splits
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

" Better whitespace config
let g:better_whitespace_enabled=1
let g:strip_whitespace_on_save=1
let g:strip_whitelines_at_eof=1
let g:current_line_whitespace_disabled_hard=1
autocmd BufWritePre * :StripWhitespace"

" Tools
function! Jsonp()
    %!python3 -m json.tool
endfunction
com! Jsonp call Jsonp()

" Add Lua configuration
source $HOME/.config/nvim/init.lua.vim
