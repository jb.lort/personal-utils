lua <<EOF

vim.g.loaded_netrw = 1
vim.g.loaded_netrwPlugin = 1

-- Setup packer
local use = require("packer").use
require("packer").startup(function()
  use 'wbthomason/packer.nvim' -- Package manager

  use {
    "nvim-lualine/lualine.nvim", -- status line
    requires = { "kyazdani42/nvim-web-devicons", opt = true }
  }

  use {
    'kdheepak/tabline.nvim',
    config = function()
      require'tabline'.setup {
        -- Defaults configuration options
        enable = true,
        options = {
        -- If lualine is installed tabline will use separators configured in lualine by default.
        -- These options can be used to override those settings.
          section_separators = {'', ''},
          component_separators = {'', ''},
          max_bufferline_percent = 66, -- set to nil by default, and it uses vim.o.columns * 2/3
          show_tabs_always = false, -- this shows tabs only when there are more than one tab or if the first tab is named
          show_devicons = true, -- this shows devicons in buffer section
          show_bufnr = false, -- this appends [bufnr] to buffer section,
          show_filename_only = false, -- shows base filename only instead of relative path in filename
          modified_icon = "+ ", -- change the default modified icon
          modified_italic = false, -- set to true by default; this determines whether the filename turns italic if modified
          show_tabs_only = false, -- this shows only tabs instead of tabs + buffers
        }
      }
      vim.cmd[[
        set guioptions-=e " Use showtabline in gui vim
        set sessionoptions+=tabpages,globals " store tabpages and globals in session
      ]]
    end,
    requires = { { 'hoob3rt/lualine.nvim', opt=true }, {'kyazdani42/nvim-web-devicons', opt = true} }
  }

  use {
    "klen/nvim-config-local",
    config = function()
      require('config-local').setup {
        -- Default options (optional)

        -- Config file patterns to load (lua supported)
        config_files = { ".nvim.lua", ".nvimrc"},

        -- Where the plugin keeps files data
        hashfile = vim.fn.stdpath("data") .. "/config-local",

        autocommands_create = true, -- Create autocommands (VimEnter, DirectoryChanged)
        commands_create = true,     -- Create commands (ConfigLocalSource, ConfigLocalEdit, ConfigLocalTrust, ConfigLocalIgnore)
        silent = false,             -- Disable plugin messages (Config loaded/ignored)
        lookup_parents = false,     -- Lookup config files in parent directories
      }
    end
  }
end)

-- Mappings.
-- See `:help vim.diagnostic.*` for documentation on any of the below functions
local opts = { noremap=true, silent=true }
vim.keymap.set('n', '<space>e', vim.diagnostic.open_float, opts)
vim.keymap.set('n', '[d', vim.diagnostic.goto_prev, opts)
vim.keymap.set('n', ']d', vim.diagnostic.goto_next, opts)
vim.keymap.set('n', '<space>q', vim.diagnostic.setloclist, opts)

-- Setup treesitter
require'nvim-treesitter.configs'.setup {
    -- Select parsers
    ensure_installed = { "bash", "c", "cmake", "cpp", "python" },
    -- Install parsers synchronously (only applied to `ensure_installed`)
    sync_install = true,
    auto_install = true,
    highlight = {
    	enable = true
    },
    autotag = {
  		enable = true
    }
}

require("trouble").setup()
require("lualine").setup()
require("Comment").setup()
require("nvim-tree").setup()
require("goto-preview").setup()

vim.keymap.set(
    'n',
    '<leader>gp',
    "<cmd>lua require('goto-preview').goto_preview_definition()<CR>",
    opts
)

-- Setup lspkind-nvim
local lspkind = require("lspkind")

require("plugins_configs")
require("lsp_configs")

EOF
