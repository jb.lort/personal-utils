OK_NVIMTREE, NVIMTREE = pcall(require, "nvim-tree")
if not OK_NVIMTREE then
    print('"nvim-tree" not available')
    return
end

NVIMTREE_BUILTIN = require("nvim-tree")
