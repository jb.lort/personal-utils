OK_TELESCOPE, TELESCOPE = pcall(require, "telescope")
if not OK_TELESCOPE then
    print('"nvim-telescope/telescope.nvim" not available')
    return
end

TELESCOPE_BUILTIN = require("telescope.builtin")

local project_files = function()
    local ok_git_files = pcall(
        TELESCOPE_BUILTIN.git_files,
        { show_untracked = true }
    )
    if not ok_git_files then
        TELESCOPE_BUILTIN.find_files({})
    end
end

local glob_live_grep = function()
  local opts = {} -- define here if you want to define something
  vim.fn.system('git rev-parse --is-inside-work-tree')
  if vim.v.shell_error == 0 then
    -- Need to strip the last two chars for the tree root as this ends up being a non-interpreted "\n"
    local git_tree_root = string.sub(vim.fn.system('git rev-parse --show-toplevel'), 0, -2)
    opts = {
        cwd = git_tree_root
    }
    TELESCOPE_BUILTIN.live_grep(opts)
  else
    TELESCOPE_BUILTIN.live_grep({})
  end
end

local glob_live_syms = function()
    TELESCOPE_BUILTIN.treesitter()
end

vim.keymap.set({ "n" }, "<leader>ff", function()
    TELESCOPE_BUILTIN.find_files()
end, NOREMAP_SILENT)
vim.keymap.set({ "n" }, "<leader>fp", function()
    project_files()
end, NOREMAP_SILENT)
vim.keymap.set({ "n" }, "<leader>fg", function()
    glob_live_grep()
end, NOREMAP_SILENT)
vim.keymap.set({ "n" }, "<leader>fG", function()
    TELESCOPE_BUILTIN.live_grep({})
end, NOREMAP_SILENT)
vim.keymap.set({ "n" }, "<leader>ls", function()
    TELESCOPE_BUILTIN.buffers()
end, NOREMAP_SILENT)
vim.keymap.set({ "n" }, "<leader>fs", function()
    glob_live_syms()
end, NOREMAP_SILENT)
